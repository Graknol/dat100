package oving1;

import utils.ConsoleColors;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Summation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        long sum = 0;
        long input = 0;

        System.out.print(ConsoleColors.CYAN + "Skriv inn et positivt heltall (eller 0 for å avslutte): " + ConsoleColors.RESET);

        while ((input = scanner.nextLong()) > 0) {
            sum += input;
            System.out.println(ConsoleColors.YELLOW + "Den nye summen er: " + ConsoleColors.BLUE + sum + "\n");
            System.out.print(ConsoleColors.CYAN + "Skriv inn et nytt positivt heltall (eller 0 for å avslutte): " + ConsoleColors.RESET);
        }

        System.out.println(ConsoleColors.PURPLE + "Den endelige summen ble: " + ConsoleColors.BLUE + sum + ConsoleColors.RESET);
    }
}
