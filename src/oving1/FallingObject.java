package oving1;

import utils.ConsoleColors;

import java.text.DecimalFormat;
import java.util.Scanner;

public class FallingObject {

    private final static DecimalFormat decimalFormatter = new DecimalFormat("#0.000");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(ConsoleColors.YELLOW +
                "Dette programmet skal regne ut fart og distanse for et\n" +
                "objekt som faller med 9.81m/s^2 i n-antall intervaller\n" +
                "spesifisert av brukeren.");

        System.out.print(ConsoleColors.BLUE + "\nTidsskritt for hvert intervall: ");
        double intervalStep = scanner.nextDouble();

        if (intervalStep <= 0) {
            System.out.println(ConsoleColors.RED + "Tidsskritt for hvert intervall må være større enn " + ConsoleColors.PURPLE + "0." + ConsoleColors.RESET + "\n");
            scanner.close();
            return;
        }

        System.out.print("Antall intervall: ");
        int intervalCount = scanner.nextInt();

        if (intervalCount <= 0) {
            System.out.println(ConsoleColors.RED + "Antall intervall må være større enn " + ConsoleColors.PURPLE + "0." + ConsoleColors.RESET + "\n");
            scanner.close();
            return;
        }

        scanner.close();

        for (int i = 0; i < intervalCount; i++) {
            double seconds = (intervalStep * (i + 1));
            double speed = 9.81 * seconds;
            double distance = 0.5 * speed * seconds;
            System.out.println(ConsoleColors.CYAN +
                    "Intervall #" + (i + 1) + " - " +
                    ConsoleColors.GREEN +
                    "Fart: " + decimalFormatter.format(speed) +
                    " og distanse: " + decimalFormatter.format(distance) +
                    ConsoleColors.RESET);
        }
    }
}
