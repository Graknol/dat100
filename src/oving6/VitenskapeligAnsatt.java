package oving6;

import java.util.ArrayList;
import java.util.List;

public class VitenskapeligAnsatt extends Ansatt {

    private String fagfelt = "";
    private List<Emne> undervisningEmner = new ArrayList<>();

    public VitenskapeligAnsatt(int ansattnummer, int ID, String firstName, String lastName, int yearOfBirth) {
        super(ansattnummer, ID, firstName, lastName, yearOfBirth);
    }

    public String getFagfelt() {
        return fagfelt;
    }

    public VitenskapeligAnsatt setFagfelt(String fagfelt) {
        this.fagfelt = fagfelt;
        return this;
    }

    public List<Emne> getUndervisningEmner() {
        return undervisningEmner;
    }

    public VitenskapeligAnsatt addUndervisningEmne(Emne emne) {
        undervisningEmner.add(emne);
        return this;
    }

    @Override
    public String toString() {
        return "VitenskapeligAnsatt - " +
                "fagfelt='" + fagfelt + '\'' +
                ", undervisningEmner=" + undervisningEmner;
    }
}
