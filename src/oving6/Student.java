package oving6;

import oving4.Person;

import java.util.ArrayList;
import java.util.List;

public class Student extends Person {

    private String studieprogram = "";
    private int arskurs = 0;
    private List<Emne> emner = new ArrayList<>();

    public Student(int ID, String firstName, String lastName, int yearOfBirth) {
        super(ID, firstName, lastName, yearOfBirth);
    }

    public String getStudieprogram() {
        return studieprogram;
    }

    public Student setStudieprogram(String studieprogram) {
        this.studieprogram = studieprogram;
        return this;
    }

    public int getArskurs() {
        return arskurs;
    }

    public Student setArskurs(int arskurs) {
        this.arskurs = arskurs;
        return this;
    }

    public List<Emne> getEmner() {
        return emner;
    }

    public Student addEmne(Emne emne) {
        emner.add(emne);
        return this;
    }

    @Override
    public String toString() {
        return "Student \t\t\t- " +
                "studieprogram='" + studieprogram + '\'' +
                ", arskurs=" + arskurs +
                ", emner=" + emner;
    }
}
