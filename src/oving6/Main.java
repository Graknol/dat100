package oving6;

import oving4.Person;
import utils.ConsoleColors;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ArrayList<Person> persons = new ArrayList<>(8);

        addSamplePersons(persons);

        System.out.println(ConsoleColors.CYAN + "\nInfo om alle personene:" + ConsoleColors.YELLOW);
        printAllPersons(persons);
    }

    private static void printAllPersons(List<Person> persons) {
        for (Person person : persons) {
            System.out.println(person.toString());
        }
    }

    private static void addSamplePersons(List<Person> persons) {
        // Ansatt
        persons.add(new Ansatt(1, 131, "Lars", "Djupevåg", 1963)
                .setArslonn(450000)
                .setStilling("Profesjonell Alteter")
        );
        persons.add(new Ansatt(2, 147, "Merse", "Desbens", 1985)
                .setArslonn(743000)
                .setStilling("Chief Executive Sleeper")
        );

        // Student
        persons.add(new Student(131, "Sindre", "Tellevik", 1998)
                .setStudieprogram("Informasjonsteknologi")
                .setArskurs(1)
                .addEmne(new Emne("DAT100", "Objektorientert Programmering", 'V'))
                .addEmne(new Emne("MAT200", "Matematiske Metoder 2", 'V'))
                .addEmne(new Emne("STA100", "Sannsynlighetsregning og statistikk 1", 'V'))
        );
        persons.add(new Student(147, "Stian", "Vedvik", 1998)
                .setStudieprogram("Informasjonsteknologi")
                .setArskurs(1)
                .addEmne(new Emne("DAT100", "Objektorientert Programmering", 'V'))
                .addEmne(new Emne("MAT200", "Matematiske Metoder 2", 'V'))
                .addEmne(new Emne("STA100", "Sannsynlighetsregning og statistikk 1", 'V'))
        );

        // VitenskapeligAnsatt
        persons.add(new VitenskapeligAnsatt(5, 169, "Veting", "Enting", 1986)
                .setFagfelt("Botanikk")
                .addUndervisningEmne(new Emne("BOT230", "Botaniske Effekter i Verden 2", 'H'))
                .setArslonn(430000)
                .setStilling("Førsteamanuensis")
        );
        persons.add(new VitenskapeligAnsatt(6, 210, "Hans", "Vetjeg", 1939)
                .setFagfelt("Eldrekunnskap")
                .addUndervisningEmne(new Emne("ELD900", "Eldrekunnskap 9", 'V'))
                .setArslonn(980000)
                .setStilling("Underviser")
        );

        // TekniskAnsatt
        persons.add(new TekniskAnsatt(7, 170, "Liva", "Sandven", 1990)
                .setAnsvarligForRom(new Rom('K', 2, 204, 45.7))
                .setArslonn(320000)
                .setStilling("Romansvarlig")
        );
        persons.add(new TekniskAnsatt(8, 211, "Ola", "Normann", 1991)
                .setAnsvarligForRom(new Rom('E', 3, 312, 12.5))
                .setArslonn(335000)
                .setStilling("Romansvarlig")
        );
    }
}
