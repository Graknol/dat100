package oving6;

import oving4.Person;

public class Ansatt extends Person {

    private int ansattnummer;
    private String stilling = "";
    private int arslonn = 0;

    public Ansatt(int ansattnummer, int ID, String firstName, String lastName, int yearOfBirth) {
        super(ID, firstName, lastName, yearOfBirth);
        this.ansattnummer = ansattnummer;
    }

    public int getAnsattnummer() {
        return ansattnummer;
    }

    public String getStilling() {
        return stilling;
    }

    public Ansatt setStilling(String stilling) {
        this.stilling = stilling;
        return this;
    }

    public int getArslonn() {
        return arslonn;
    }

    public Ansatt setArslonn(int arslonn) {
        this.arslonn = arslonn;
        return this;
    }

    @Override
    public String toString() {
        return "Ansatt \t\t\t\t- " +
                "ansattnummer=" + ansattnummer +
                ", stilling='" + stilling + '\'' +
                ", arslonn=" + arslonn;
    }
}
