package oving6;

import java.util.ArrayList;
import java.util.List;

public class TekniskAnsatt extends Ansatt {

    private Rom ansvarligForRom;

    public TekniskAnsatt(int ansattnummer, int ID, String firstName, String lastName, int yearOfBirth) {
        super(ansattnummer, ID, firstName, lastName, yearOfBirth);
    }

    public Rom getAnsvarligForRom() {
        return ansvarligForRom;
    }

    public TekniskAnsatt setAnsvarligForRom(Rom rom) {
        this.ansvarligForRom = rom;
        return this;
    }

    @Override
    public String toString() {
        return "TekniskAnsatt \t\t- " +
                "ansvarligFor" + ansvarligForRom;
    }
}
