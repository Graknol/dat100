package oving3;

import utils.ConsoleColors;

import java.util.Scanner;

public class Oving3 {

    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);

        Spiller[] spillere = new Spiller[2];
        for (int i = 0; i < 2; i++) {
            System.out.println("Skriv inn ID på spiller: ");
            int id = inn.nextInt();
            inn.nextLine();

            System.out.println("Skriv inn navn på spiller: ");
            String name = inn.nextLine();

            System.out.println("Skriv inn poengsum til spiller: ");
            int score = inn.nextInt();
            inn.nextLine();

            Spiller spiller = new Spiller(id, name);
            spiller.setPoengsum(score);
            System.out.println(spiller.toString());

            spillere[i] = spiller;
        }

        Spiller swhs = Spiller.spillerWithHighestScore(spillere);
        System.out.println(ConsoleColors.GREEN + "Spiller med høyest poengsum er:");
        System.out.println(ConsoleColors.BLUE + swhs.toString());
    }
}
