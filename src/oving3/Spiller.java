package oving3;

public class Spiller {
    private int ID;
    private String navn;
    private int poengsum = 0;
    private boolean givenUp = false;
    private int position = 0;

    public Spiller(int ID, String navn) {
        this.ID = ID;
        this.navn = navn;
    }

    public int getID() {
        return ID;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        if (navn.length() > 0) {
            this.navn = navn;
        }
    }

    public int getPoengsum() {
        return poengsum;
    }

    public void setPoengsum(int poengsum) {
        if (poengsum >= 0) {
            this.poengsum = poengsum;
        }
    }

    public boolean getGivenUp() {
        return givenUp;
    }

    public void giveUp() {
        givenUp = true;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Spiller '" + navn + '\'' +
                " (ID: " + ID + ')' +
                " har poengsum: " + poengsum;
    }

    public static Spiller spillerWithHighestScore(Spiller... players) {
        Spiller playerResult = players[0];

        for (int i = 1; i < players.length; i++) {
            if (players[i].poengsum > playerResult.poengsum) {
                playerResult = players[i];
            }
        }

        return playerResult;
    }
}
