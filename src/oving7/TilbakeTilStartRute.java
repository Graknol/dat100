package oving7;

import oving3.Spiller;

public class TilbakeTilStartRute extends AbstraktRute {

    public TilbakeTilStartRute(int position) {
        super(position);
    }

    @Override
    public void flyttHit(Spiller player) {
        player.setPosition(0);
        System.out.println(player.getNavn() + " prøvde å flytte til rute " + this.getPosition() + ", men må rykke tilbake til start.");
    }
}
