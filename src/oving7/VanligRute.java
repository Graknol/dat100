package oving7;

import oving3.Spiller;

public class VanligRute extends AbstraktRute {

    public VanligRute(int position) {
        super(position);
    }

    @Override
    public void flyttHit(Spiller player) {
        player.setPosition(this.getPosition());
        System.out.println(player.getNavn() + " er flyttet til rute " + this.getPosition());
    }
}
