package oving7;

import oving3.Spiller;
import oving5.Terning;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Rute[] ruter = new Rute[20];
        Random rand = new Random();

        boolean[] ruteOpptatt = new boolean[ruter.length];
        int vanligeRuter = 14;
        int tilbakeTilStartRuter = ruter.length - vanligeRuter;
        while (vanligeRuter > 0 || tilbakeTilStartRuter > 0) {
            int idx = rand.nextInt(ruter.length);
            while(ruteOpptatt[idx]) {
                idx = rand.nextInt(ruter.length);
            }
            ruteOpptatt[idx] = true;
            if (vanligeRuter > 0) {
                vanligeRuter--;
                ruter[idx] = new VanligRute(idx);
            } else {
                tilbakeTilStartRuter--;
                ruter[idx] = new TilbakeTilStartRute(idx);
            }
        }

        Spiller winner = null;
        Terning terning = new Terning();

        Scanner scanner = new Scanner(System.in);

        System.out.print("Hvor mange skal spille: ");
        Spiller[] spillere = new Spiller[scanner.nextInt()];
        scanner.nextLine();

        for (int i = 0; i < spillere.length; i++){
            System.out.print("Navn på spiller " + i + ": ");
            spillere[i] = new Spiller(i, scanner.nextLine());
        }

        System.out.println("La oss begynne...\n");
        while(winner == null) {
            for (Spiller spiller : spillere) {
                System.out.println("Det er " + spiller.getNavn() + " sin tur. Du står i rute " + spiller.getPosition());
                System.out.print("Kast terningen!");
                scanner.nextLine();

                terning.kast();
                if (spiller.getPosition() + terning.getTall() >= ruter.length) {
                    winner = spiller;
                    break;
                }
                ruter[spiller.getPosition() + terning.getTall()].flyttHit(spiller);
                System.out.println();
            }
        }

        System.out.println(winner.getNavn() + " vant, gratulerer!");
    }
}
