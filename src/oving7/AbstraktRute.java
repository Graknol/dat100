package oving7;

import oving3.Spiller;

public abstract class AbstraktRute implements Rute {
    private int position;

    public AbstraktRute(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
