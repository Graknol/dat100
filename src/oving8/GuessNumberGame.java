package oving8;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.util.Random;

public class GuessNumberGame extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    Random rand = new Random();
    int secretNumber;
    int previousGuessedNumber = 0;
    int guessCount = 0;

    @Override
    public void start(Stage primaryStage) {
        secretNumber = rand.nextInt(1000) + 1;

        try {
            primaryStage.setTitle("Gjett tallet spill");

            GridPane root = new GridPane();
            root.setAlignment(Pos.CENTER_LEFT);
            root.setHgap(8);
            root.setVgap(8);
            root.setPadding(new Insets(16, 16, 16, 16));

            GridPane inputRow = new GridPane();
            inputRow.setAlignment(Pos.CENTER_LEFT);
            inputRow.setHgap(8);
            inputRow.setVgap(8);
            inputRow.setPadding(new Insets(8, 8, 8, 8));
            root.add(inputRow, 0, 0);

            Label tittel = new Label("Gjett et tall fra 1 til 1000: ");
            inputRow.add(tittel, 0, 0);

            TextField inputField = new TextField();
            inputRow.add(inputField, 1, 0);

            Label responseText = new Label("");
            root.add(responseText, 0, 1);

            Button btn = new Button("OK");
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        int number = convertInput(inputField.getText());
                        guessCount++;

                        String responseStr = "I ditt " + guessCount + " forsøk gjettet du et ";

                        if (number == secretNumber) {
                            Dialog dialog = new Dialog();
                            dialog.setTitle("Gratulerer!");
                            dialog.setContentText("Du har gjettet riktig!\nDu brukte " + guessCount + " forsøk.");
                            dialog.showAndWait();
                            previousGuessedNumber = 0;
                            guessCount = 0;
                            responseText.setText("");
                            inputField.setText("");
                            secretNumber = rand.nextInt(1000) + 1;
                            return;
                        }
                        if (number > secretNumber) {
                            responseStr += "høyere";
                        } else {
                            responseStr += "lavere";
                        }
                        responseStr += " tall enn mitt tall";
                        if (previousGuessedNumber != 0) {
                            int numberDist = Math.abs(number - secretNumber);
                            int prevNumberDist = Math.abs(previousGuessedNumber - secretNumber);
                            if (numberDist < prevNumberDist) {
                                responseStr += " og ditt gjett er nærmere enn sist gang";
                            } else if (numberDist > prevNumberDist) {
                                responseStr += " og ditt gjett er lengre unna enn sist gang";
                            }
                        }
                        responseText.setText(responseStr);
                        previousGuessedNumber = number;
                    } catch (NumberFormatException e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Feil input");
                        alert.setContentText("Input kan kun bestå av tall");
                        alert.showAndWait();
                    }catch (IllegalArgumentException e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Feil input");
                        alert.setContentText(e.getMessage());
                        alert.showAndWait();
                    }
                }
            });
            root.add(btn, 0, 2);

            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setWidth(550);
            primaryStage.show();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private int convertInput(String str) {
        int number = Integer.parseInt(str);
        if (number < 1 || number > 1000) {
            throw new IllegalArgumentException("Input må være mellom 1 (inklusiv) og 1000 (inklusiv).");
        }
        return number;
    }
}
