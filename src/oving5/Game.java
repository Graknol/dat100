package oving5;


import oving3.Spiller;

import java.util.Random;
import java.util.Scanner;

public class Game {

    private Scanner scanner;
    private Random rand = new Random();
    private Spiller[] players;

    public Game() {
        scanner = new Scanner(System.in);
    }

    public void setupGame() {
        System.out.print("Hvor mange spillere skal spille: ");
        players = new Spiller[scanner.nextInt()];

        // This is to consume the newline character left hanging by nextInt().
        // This is a "bug" in the Scanner class.
        scanner.nextLine();

        for (int i = 0; i < players.length; i++) {
            System.out.print("Navn på spiller " + (i + 1) + ": ");
            addPlayer(i, scanner.nextLine());
        }
    }

    // Det er mye tryggere om klassen håndterer dette selv gjennom setupGame.
    // For da kan jeg også fjerne mange ekstra egenskaper.
    // I tillegg forhindrer jeg OutOfRangeException dersom noen andre prøver å
    // legge til flere spillere enn oppgitt.
    // Jeg har også tatt meg friheten å tilpasse addPlayer() til å passe bedre
    // inn med setupGame(). Hvilket selvfølgelig er gjort bevisst.
    private Spiller addPlayer(int idx, String name) {
        Spiller player = new Spiller(idx, name);
        for (int i = 0; i < 3; i++) {
            player.setPoengsum(player.getPoengsum() + (rand.nextInt(6) + 1));
        }
        players[idx] = player;
        return player;
    }

    public void playOneRoundForAllPlayers() {
        for (int i = 0; i < players.length; i++) {
            playForPlayer(players[i]);
        }
    }

    private void playForPlayer(Spiller player) {
        if (canPlayerStillPlay(player)) {
            System.out.println("\nDet er " + player.getNavn() + " sin tur. "
                    + "Du har " + player.getPoengsum() + " poeng.");

            System.out.print("Ønsker du å kaste terningen én gang til? (y/n): ");
            String line = scanner.nextLine();
            if (line.contains("n")) {
                System.out.println("Du har nå gitt deg med poengsum: " + player.getPoengsum());
                player.giveUp();
            } else {
                player.setPoengsum(player.getPoengsum() + (rand.nextInt(6) + 1));
                if (player.getPoengsum() > 21) {
                    System.out.println("Du har fått over 21 poeng, og har dermed tapt.");
                }
            }
        }
    }

    private boolean canPlayerStillPlay(Spiller player) {
        return !player.getGivenUp() && player.getPoengsum() <= 21;
    }

    public Spiller getWinner() {
        int stillPlayingCount = 0;
        Spiller lowestDifferencePlayer = null;
        for (Spiller player : players) {
            if (canPlayerStillPlay(player)) {
                stillPlayingCount++;
            }

            if (lowestDifferencePlayer == null) {
                lowestDifferencePlayer = player;
            } else if (player.getPoengsum() <= 21
                    && ((21 - player.getPoengsum()) < (21 - lowestDifferencePlayer.getPoengsum()))) {
                lowestDifferencePlayer = player;
            }

            if (player.getPoengsum() == 21) {
                return player;
            }
        }

        if (stillPlayingCount == 0) {
            return lowestDifferencePlayer;
        }

        return null;
    }

    public void shutdown() {
        scanner.close();
    }
}
