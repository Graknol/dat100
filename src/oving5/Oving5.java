package oving5;

import oving3.Spiller;

public class Oving5 {

    public static void main(String[] args) {
        Game game = new Game();

        game.setupGame();

        Spiller winner = null;
        while ((winner = game.getWinner()) == null) {
            game.playOneRoundForAllPlayers();
        }

        game.shutdown();

        System.out.println();

        if (winner.getPoengsum() == 21) {
            System.out.println(winner.getNavn() + " fikk 21 poeng og vant!");
        } else {
            System.out.println(winner.getNavn() + " vant med nærmest poengsum: " + winner.getPoengsum());
        }
    }
}
