package oving2.test;

import org.junit.jupiter.api.Test;
import oving2.DateValidation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestDateValidation {

    @Test
    void testIsValidDate() {
        assertTrue(DateValidation.isValidDate(1));
        assertTrue(DateValidation.isValidDate(5));
        assertTrue(DateValidation.isValidDate(31));
        assertFalse(DateValidation.isValidDate(0));
        assertFalse(DateValidation.isValidDate(-12));
        assertFalse(DateValidation.isValidDate(32));
        assertFalse(DateValidation.isValidDate(104));
    }
}
