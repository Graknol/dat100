package oving2;

import utils.ConsoleColors;

import java.util.Scanner;

public class TriangleArea {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(ConsoleColors.YELLOW + "Merk: Alle inputverdier blir tatt absoluttverdien av.");
        System.out.print(ConsoleColors.BLUE + "Lengden til side a: ");
        double a = Math.abs(scanner.nextDouble());

        System.out.print(ConsoleColors.BLUE + "Lengden til side b: ");
        double b = Math.abs(scanner.nextDouble());

        System.out.print(ConsoleColors.BLUE + "Vinkelen mellom dem: ");
        double angle = Math.abs(scanner.nextDouble());

        double area = calculateArea(a, b, angle);
        System.out.println("Arealet til trekanten er: " + area);
    }

    private static double calculateArea(double a, double b, double angle) {
        return 0.5 * a * b * Math.sin(angle);
    }
}
