package oving2;

public class Divisible {

    /**
     *
     * @return true if a is divisible by b
     */
    public static boolean isDivisible(long a, long b) {
        return a % b == 0;
    }
}
