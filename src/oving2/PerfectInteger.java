package oving2;

import utils.ConsoleColors;

import java.util.Scanner;

public class PerfectInteger {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(ConsoleColors.YELLOW + "Programmet sjekker om det gitte positive heltallet er et perfekt tall.");

        System.out.print(ConsoleColors.BLUE + "Positivt heltall å sjekke: ");
        long integerToCheck = scanner.nextInt();

        if (integerToCheck <= 0) {
            System.out.println(ConsoleColors.RED + "Det må være et positivt heltall.");
            scanner.close();
            return;
        }

        scanner.close();

        if (isPerfectInteger(integerToCheck)) {
            System.out.println(ConsoleColors.GREEN + "Tallet " + ConsoleColors.CYAN + integerToCheck + ConsoleColors.GREEN + " er et perfekt tall!");
        }else {
            System.out.println(ConsoleColors.PURPLE + "Tallet " + ConsoleColors.CYAN + integerToCheck + ConsoleColors.PURPLE + " er ikke et perfekt tall.");
        }
    }

    public static boolean isPerfectInteger(long a) {
        long sum = 0;
        for (long i = 1; i <= a / 2; i++) {
            if (Divisible.isDivisible(a, i)) {
                sum += i;
            }
        }

        return a == sum;
    }
}
