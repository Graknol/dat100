package oving4;

public class Oving4_a {

    public static void main(String[] args) {
        float[] floatValues = {0.1f, 2.3f, 6.5f, 3.0f};
        float[] newValues = multiplyArrayByScalar(floatValues, 2.0f);

        System.out.print("The new values are: ");
        for (int i = 0; i < newValues.length; i++) {
            System.out.print(newValues[i] + " ");
        }
    }

    public static float[] multiplyArrayByScalar(float[] arr, float scalar) {
        float[] result = new float[arr.length];
        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i] * scalar;
        }
        return result;
    }
}
