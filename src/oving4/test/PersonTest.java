package oving4.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import oving4.Person;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonTest {

    Person person;

    @BeforeEach
    public void setUp() {
        person = new Person(12, "Sindre", "Tellevik", 1998);
    }

    @Test
    public void testGetters() {
        assertEquals(12, person.getID());
        assertEquals("Sindre", person.getFirstName());
        assertEquals("Tellevik", person.getLastName());
        assertEquals(1998, person.getYearOfBirth());
    }

    @Test
    public void testSetters() {
        person.setFirstName("Stian");
        person.setLastName("Vedvik");
        assertEquals("Stian", person.getFirstName());
        assertEquals("Vedvik", person.getLastName());
    }

    @Test
    public void testToString() {
        assertEquals("Person (ID=12): Sindre Tellevik (1998)", person.toString());
    }
}
