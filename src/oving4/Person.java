package oving4;

public class Person {

    private int ID;
    private String firstName;
    private String lastName;
    private int yearOfBirth;

    public Person(int ID, String firstName, String lastName, int yearOfBirth) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
    }

    public int getID() {
        return ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Person (ID=" + ID + "): " + firstName + " " + lastName + " (" + yearOfBirth + ")";
    }
}
