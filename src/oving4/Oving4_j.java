package oving4;

import utils.ConsoleColors;

import java.io.Console;
import java.time.LocalDate;
import java.util.ArrayList;

public class Oving4_j {

    public static void main(String[] args) {
        Hendelse h1 = new Hendelse(LocalDate.of(2018, 3, 20), LocalDate.of(2018, 3, 22), "Fest hos Magne");
        Hendelse h2 = new Hendelse(LocalDate.of(2018, 2, 1), LocalDate.of(2018, 2, 1), "Sindres 20års-dag");
        Person p1 = new Person(1, "Sindre", "Tellevik", 1998);
        Person p2 = new Person(2, "Kaylee", "van der Linden", 1998);
        Person p3 = new Person(3, "Stian", "Vedvik", 1997);

        h1.leggTilPerson(p1);
        h1.leggTilPerson(p3);

        h2.leggTilPerson(p1);
        h2.leggTilPerson(p2);
        h2.leggTilPerson(p3);

        ArrayList<Hendelse> hendelser = new ArrayList<>(2);
        hendelser.add(h1);
        hendelser.add(h2);

        for (Hendelse h : hendelser) {
            System.out.println(ConsoleColors.YELLOW + h.toString());
            System.out.println(ConsoleColors.GREEN + "Inviterte gjester:");
            h.getInviterteGjester().forEach(gjest -> {
                System.out.println(ConsoleColors.CYAN + "  - " + gjest.toString());
            });
            System.out.println(ConsoleColors.RESET);
        }
    }
}
