package oving9;

import javafx.scene.Node;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class Gang extends LabyrintRute {

    public Gang(int x, int y) {
        super(x, y);
    }

    @Override
    public Node getAppearance() {
        return new Rectangle(SQUARE_WIDTH, SQUARE_HEIGHT, Paint.valueOf("#ECEFF1"));
    }
}
