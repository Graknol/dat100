package oving9;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open map file");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Map Files", "*.map")
        );
        File selectedFile = fileChooser.showOpenDialog(primaryStage);

        if (selectedFile != null) {
            Player player = new Player(0, 0);
            Node playerNode = player.getAppearance();

            LabyrintRute[][] map = Utils.readRouteFile(selectedFile.toPath());
            int width = map.length;
            int height = map[0].length;

            GridPane root = new GridPane();
            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[i].length; j++) {
                    LabyrintRute square = map[i][j];

                    root.add(square.getAppearance(), i, j);

                    if (square instanceof Start) {
                        player.setPosition(square.getX(), square.getY());
                        root.add(playerNode, player.getX(), player.getY());
                    }
                }
            }

            Scene scene = new Scene(root);

            // Victory dialog
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Gratulerer, du vant!");
            alert.setTitle("Wohoo!");

            scene.setOnKeyPressed(event -> {
                int x = player.getX();
                int y = player.getY();

                switch (event.getCode()) {
                    case LEFT:
                        if (!(map[x - 1][y] instanceof Vegg))
                            x = Math.max(x - 1, 0);
                        break;
                    case RIGHT:
                        if (!(map[x + 1][y] instanceof Vegg))
                            x = Math.min(x + 1, width - 1);
                        break;
                    case UP:
                        if (!(map[x][y - 1] instanceof Vegg))
                            y = Math.max(y - 1, 0);
                        break;
                    case DOWN:
                        if (!(map[x][y + 1] instanceof Vegg))
                            y = Math.min(y + 1, height - 1);
                        break;
                }

                player.setPosition(x, y);
                GridPane.setColumnIndex(playerNode, x);
                GridPane.setRowIndex(playerNode, y);
                playerNode.toFront();

                if (map[x][y] instanceof Utgang) {
                    alert.showAndWait();
                    try {
                        Platform.exit();
                        System.exit(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            primaryStage.setScene(scene);
            primaryStage.sizeToScene();
            primaryStage.show();
        }
    }
}
