package oving9;

import javafx.scene.Node;

public abstract class LabyrintRute {
    public static final int SQUARE_WIDTH = 16;
    public static final int SQUARE_HEIGHT = 16;

    private int x, y;

    public LabyrintRute(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public abstract Node getAppearance();
}
