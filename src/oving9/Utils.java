package oving9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Utils {
    public static LabyrintRute[][] readRouteFile(Path filePath) {
        LabyrintRute[][] squares = null;

        try {
            squares = parseLines(Files.readAllLines(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return squares;
    }

    private static LabyrintRute[][] parseLines(List<String> lines) {
        LabyrintRute[][] squares = null;

        if (lines.size() >= 2) {
            int width = Integer.parseInt(lines.get(0));
            int height = Integer.parseInt(lines.get(1));

            squares = new LabyrintRute[width][height];

            for (int i = 2; i < (2 + height); i++) {
                String line = lines.get(i).toUpperCase();
                for (int j = 0; j < width; j++) {
                    int x = j;
                    int y = i - 2;
                    switch (line.charAt(j)) {
                        case '#':
                            squares[x][y] = new Vegg(x, y);
                            break;
                        case ' ':
                            squares[x][y] = new Gang(x, y);
                            break;
                        case '*':
                            squares[x][y] = new Start(x, y);
                            break;
                        case '-':
                            squares[x][y] = new Utgang(x, y);
                            break;
                    }
                }
            }
        }

        return squares;
    }
}
